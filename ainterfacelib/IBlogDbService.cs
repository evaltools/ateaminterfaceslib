﻿using System.Collections.Generic;

namespace ainterfacelib
{
    public interface IBlogDbService
    {
        int CreateBlog(Blog blog);
        Blog ReadBlog(int blogId);
        int UpdateBlog(Blog blog);
        int DeleteBlog(int blogId);

        int CreatePost(Post post);
        Post ReadPost(int postId);
        int UpdatePost(Post blog);
        int DeletePost(int postId);
    }

    public class Blog
    {
        public string Url { get; set; }
        public int Rating { get; set; }
        public List<Post> Posts { get; set; }
    }

    public class Post
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
